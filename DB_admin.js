var MongoClient = require('mongodb').MongoClient;
var url = "mongodb+srv://user:pass@cluster0-qqgvm.mongodb.net/";

function delete_all(collection) {
  MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    var dbo = db.db("fifadb");
    var myquery = {};
    dbo.collection(collection).deleteMany(myquery, function(err, obj) {
      if (err) throw err;
      console.log(obj.result.n + " document(s) deleted");
      db.close();
    });
  });
}

// delete_all("Predictions")
// delete_all("Games")
