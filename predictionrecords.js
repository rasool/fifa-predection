var DB = require('./DBfunctions');
var Parse = require('./fifa_process');

var prev_predictions = [
    {
        message:
            {
                text: '#wc2018 adminprediction 91711331 alikhadem RUS-KSA 2-0',
                chat: {id: -266372953},
                from: {id: 1111111, first_name: 'FifaBot'}
            }
    },
    {
        message:
            {
                text: '#wc2018 adminprediction 91341610 Alireza RUS-KSA 2-0',
                chat: {id: -266372953},
                from: {id: 1111111, first_name: 'FifaBot'}
            }
    },
    {
        message:
            {
                text: '#wc2018 adminprediction 202573088 Mohammadamin RUS-KSA 1-1',
                chat: {id: -266372953},
                from: {id: 1111111, first_name: 'FifaBot'}
            }
    },
    {
        message:
            {
                text: '#wc2018 adminprediction 379150329 Asal RUS-KSA 1-0',
                chat: {id: -266372953},
                from: {id: 1111111, first_name: 'FifaBot'}
            }
    },
    {
        message:
            {
                text: '#wc2018 adminprediction 105348277 Faegheh RUS-KSA 2-0',
                chat: {id: -266372953},
                from: {id: 1111111, first_name: 'FifaBot'}
            }
    },
    {
        message:
            {
                text: '#wc2018 adminprediction 340961698 Kimia RUS-KSA 2-0',
                chat: {id: -266372953},
                from: {id: 1111111, first_name: 'FifaBot'}
            }
    },
    {
        message:
            {
                text: '#wc2018 adminprediction 75043130 MahnAz RUS-KSA 2-1',
                chat: {id: -266372953},
                from: {id: 1111111, first_name: 'FifaBot'}
            }
    },
    {
        message:
            {
                text: '#wc2018 adminprediction 108881239 Mina RUS-KSA 1-2',
                chat: {id: -266372953},
                from: {id: 1111111, first_name: 'FifaBot'}
            }
    },
    {
        message:
            {
                text: '#wc2018 adminprediction 74458279 Mojtaba RUS-KSA 2-1',
                chat: {id: -266372953},
                from: {id: 1111111, first_name: 'FifaBot'}
            }
    },
    {
        message:
            {
                text: '#wc2018 adminprediction 105508663 Rasool RUS-KSA 4-1',
                chat: {id: -266372953},
                from: {id: 1111111, first_name: 'FifaBot'}
            }
    },
    {
        message:
            {
                text: '#wc2018 adminprediction 111879932 Zeinab RUS-KSA 3-1',
                chat: {id: -266372953},
                from: {id: 1111111, first_name: 'FifaBot'}
            }
    },
    {
        message:
            {
                text: '#wc2018 adminprediction 91341610 Alireza EGY-URU 1-2',
                chat: {id: -266372953},
                from: {id: 1111111, first_name: 'FifaBot'}
            }
    },
    {
        message:
            {
                text: '#wc2018 adminprediction 202573088 Mohammadamin EGY-URU 1-2',
                chat: {id: -266372953},
                from: {id: 1111111, first_name: 'FifaBot'}
            }
    },
    {
        message:
            {
                text: '#wc2018 adminprediction 379150329 Asal EGY-URU 0-2',
                chat: {id: -266372953},
                from: {id: 1111111, first_name: 'FifaBot'}
            }
    },
    {
        message:
            {
                text: '#wc2018 adminprediction 105348277 Faegheh EGY-URU 2-2',
                chat: {id: -266372953},
                from: {id: 1111111, first_name: 'FifaBot'}
            }
    },
    {
        message:
            {
                text: '#wc2018 adminprediction 108881239 Mina EGY-URU 0-0',
                chat: {id: -266372953},
                from: {id: 1111111, first_name: 'FifaBot'}
            }
    },
    {
        message:
            {
                text: '#wc2018 adminprediction 75043130 MahnAz EGY-URU 0-2',
                chat: {id: -266372953},
                from: {id: 1111111, first_name: 'FifaBot'}
            }
    },
    {
        message:
            {
                text: '#wc2018 adminprediction 105508663 Rasool EGY-URU 0-3',
                chat: {id: -266372953},
                from: {id: 1111111, first_name: 'FifaBot'}
            }
    },
    {
        message:
            {
                text: '#wc2018 adminprediction 111879932 Zeinab URU-EGY 3-0',
                chat: {id: -266372953},
                from: {id: 1111111, first_name: 'FifaBot'}
            }
    },
    {
        message:
            {
                text: '#wc2018 adminprediction 74458279 Mojtaba URU-EGY 3-1',
                chat: {id: -266372953},
                from: {id: 1111111, first_name: 'FifaBot'}
            }
    },
    {
        message:
            {
                text: '#wc2018 adminprediction 91711331 alikhadem URU-EGY 3-1',
                chat: {id: -266372953},
                from: {id: 1111111, first_name: 'FifaBot'}
            }
    },
    {
        message:
            {
                text: '#wc2018 adminprediction 340961698 Kimia URU-EGY 1-2',
                chat: {id: -266372953},
                from: {id: 1111111, first_name: 'FifaBot'}
            }
    },
    {
        message:
            {
                text: '#wc2018 adminprediction 91711331 alikhadem IRN-MAR 3-1',
                chat: {id: -266372953},
                from: {id: 1111111, first_name: 'FifaBot'}
            }
    },
    {
        message:
            {
                text: '#wc2018 adminprediction 91341610 Alireza IRN-MAR 2-0',
                chat: {id: -266372953},
                from: {id: 1111111, first_name: 'FifaBot'}
            }
    },
    {
        message:
            {
                text: '#wc2018 adminprediction 202573088 Mohammadamin IRN-MAR 2-1',
                chat: {id: -266372953},
                from: {id: 1111111, first_name: 'FifaBot'}
            }
    },
    {
        message:
            {
                text: '#wc2018 adminprediction 379150329 Asal IRN-MAR 1-2',
                chat: {id: -266372953},
                from: {id: 1111111, first_name: 'FifaBot'}
            }
    },
    {
        message:
            {
                text: '#wc2018 adminprediction 105348277 Faegheh IRN-MAR 0-0',
                chat: {id: -266372953},
                from: {id: 1111111, first_name: 'FifaBot'}
            }
    },
    {
        message:
            {
                text: '#wc2018 adminprediction 340961698 Kimia IRN-MAR 2-1',
                chat: {id: -266372953},
                from: {id: 1111111, first_name: 'FifaBot'}
            }
    },
    {
        message:
            {
                text: '#wc2018 adminprediction 75043130 MahnAz IRN-MAR 1-1',
                chat: {id: -266372953},
                from: {id: 1111111, first_name: 'FifaBot'}
            }
    },
    {
        message:
            {
                text: '#wc2018 adminprediction 108881239 Mina IRN-MAR 2-0',
                chat: {id: -266372953},
                from: {id: 1111111, first_name: 'FifaBot'}
            }
    },
    {
        message:
            {
                text: '#wc2018 adminprediction 74458279 Mojtaba IRN-MAR 2-1',
                chat: {id: -266372953},
                from: {id: 1111111, first_name: 'FifaBot'}
            }
    },
    {
        message:
            {
                text: '#wc2018 adminprediction 105508663 Rasool IRN-MAR 2-2',
                chat: {id: -266372953},
                from: {id: 1111111, first_name: 'FifaBot'}
            }
    },
    {
        message:
            {
                text: '#wc2018 adminprediction 111879932 Zeinab IRN-MAR 1-1',
                chat: {id: -266372953},
                from: {id: 1111111, first_name: 'FifaBot'}
            }
    },
    {
        message:
            {
                text: '#wc2018 adminprediction 91711331 alikhadem ESP-POR 1-1',
                chat: {id: -266372953},
                from: {id: 1111111, first_name: 'FifaBot'}
            }
    },
    {
        message:
            {
                text: '#wc2018 adminprediction 91341610 Alireza ESP-POR 3-1',
                chat: {id: -266372953},
                from: {id: 1111111, first_name: 'FifaBot'}
            }
    },
    {
        message:
            {
                text: '#wc2018 adminprediction 202573088 Mohammadamin ESP-POR 3-1',
                chat: {id: -266372953},
                from: {id: 1111111, first_name: 'FifaBot'}
            }
    },
    {
        message:
            {
                text: '#wc2018 adminprediction 379150329 Asal ESP-POR 3-1',
                chat: {id: -266372953},
                from: {id: 1111111, first_name: 'FifaBot'}
            }
    },
    {
        message:
            {
                text: '#wc2018 adminprediction 105348277 Faegheh ESP-POR 2-0',
                chat: {id: -266372953},
                from: {id: 1111111, first_name: 'FifaBot'}
            }
    },
    {
        message:
            {
                text: '#wc2018 adminprediction 340961698 Kimia ESP-POR 3-1',
                chat: {id: -266372953},
                from: {id: 1111111, first_name: 'FifaBot'}
            }
    },
    {
        message:
            {
                text: '#wc2018 adminprediction 75043130 MahnAz ESP-POR 3-2',
                chat: {id: -266372953},
                from: {id: 1111111, first_name: 'FifaBot'}
            }
    },
    {
        message:
            {
                text: '#wc2018 adminprediction 108881239 Mina ESP-POR 1-2',
                chat: {id: -266372953},
                from: {id: 1111111, first_name: 'FifaBot'}
            }
    },
    {
        message:
            {
                text: '#wc2018 adminprediction 74458279 Mojtaba ESP-POR 2-1',
                chat: {id: -266372953},
                from: {id: 1111111, first_name: 'FifaBot'}
            }
    },
    {
        message:
            {
                text: '#wc2018 adminprediction 105508663 Rasool ESP-POR 1-1',
                chat: {id: -266372953},
                from: {id: 1111111, first_name: 'FifaBot'}
            }
    },
    {
        message:
            {
                text: '#wc2018 adminprediction 111879932 Zeinab ESP-POR 2-1',
                chat: {id: -266372953},
                from: {id: 1111111, first_name: 'FifaBot'}
            }
    },
    {
        message:
            {
                text: '#wc2018 adminprediction 91711331 alikhadem FRA-AUS 1-0',
                chat: {id: -266372953},
                from: {id: 1111111, first_name: 'FifaBot'}
            }
    },
    {
        message:
            {
                text: '#wc2018 adminprediction 91341610 Alireza FRA-AUS 4-0',
                chat: {id: -266372953},
                from: {id: 1111111, first_name: 'FifaBot'}
            }
    },
    {
        message:
            {
                text: '#wc2018 adminprediction 202573088 Mohammadamin FRA-AUS 2-0',
                chat: {id: -266372953},
                from: {id: 1111111, first_name: 'FifaBot'}
            }
    },
    {
        message:
            {
                text: '#wc2018 adminprediction 379150329 Asal FRA-AUS 3-0',
                chat: {id: -266372953},
                from: {id: 1111111, first_name: 'FifaBot'}
            }
    },
    {
        message:
            {
                text: '#wc2018 adminprediction 105348277 Faegheh FRA-AUS 4-1',
                chat: {id: -266372953},
                from: {id: 1111111, first_name: 'FifaBot'}
            }
    },
    {
        message:
            {
                text: '#wc2018 adminprediction 340961698 Kimia FRA-AUS 3-0',
                chat: {id: -266372953},
                from: {id: 1111111, first_name: 'FifaBot'}
            }
    },
    {
        message:
            {
                text: '#wc2018 adminprediction 75043130 MahnAz FRA-AUS 3-1',
                chat: {id: -266372953},
                from: {id: 1111111, first_name: 'FifaBot'}
            }
    },
    {
        message:
            {
                text: '#wc2018 adminprediction 108881239 Mina FRA-AUS 2-1',
                chat: {id: -266372953},
                from: {id: 1111111, first_name: 'FifaBot'}
            }
    },
    {
        message:
            {
                text: '#wc2018 adminprediction 74458279 Mojtaba FRA-AUS 3-0',
                chat: {id: -266372953},
                from: {id: 1111111, first_name: 'FifaBot'}
            }
    },
    {
        message:
            {
                text: '#wc2018 adminprediction 105508663 Rasool FRA-AUS 5-0',
                chat: {id: -266372953},
                from: {id: 1111111, first_name: 'FifaBot'}
            }
    },
    {
        message:
            {
                text: '#wc2018 adminprediction 111879932 Zeinab FRA-AUS 3-0',
                chat: {id: -266372953},
                from: {id: 1111111, first_name: 'FifaBot'}
            }
    },
    {
        message:
            {
                text: '#wc2018 adminprediction 111879932 Zeinab ARG-ISL 2-1',
                chat: {id: -266372953},
                from: {id: 1111111, first_name: 'FifaBot'}
            }
    },
    {
        message:
            {
                text: '#wc2018 adminprediction 105508663 Rasool ARG-ISL 3-0',
                chat: {id: -266372953},
                from: {id: 1111111, first_name: 'FifaBot'}
            }
    },
    {
        message:
            {
                text: '#wc2018 adminprediction 74458279 Mojtaba ARG-ISL 3-0',
                chat: {id: -266372953},
                from: {id: 1111111, first_name: 'FifaBot'}
            }
    },
    {
        message:
            {
                text: '#wc2018 adminprediction 108881239 Mina ARG-ISL 3-0',
                chat: {id: -266372953},
                from: {id: 1111111, first_name: 'FifaBot'}
            }
    },
    {
        message:
            {
                text: '#wc2018 adminprediction 75043130 MahnAz ARG-ISL 2-1',
                chat: {id: -266372953},
                from: {id: 1111111, first_name: 'FifaBot'}
            }
    },
    {
        message:
            {
                text: '#wc2018 adminprediction 340961698 Kimia ARG-ISL 2-0',
                chat: {id: -266372953},
                from: {id: 1111111, first_name: 'FifaBot'}
            }
    },
    {
        message:
            {
                text: '#wc2018 adminprediction 105348277 Faegheh ARG-ISL 3-2',
                chat: {id: -266372953},
                from: {id: 1111111, first_name: 'FifaBot'}
            }
    },
    {
        message:
            {
                text: '#wc2018 adminprediction 379150329 Asal ARG-ISL 2-0',
                chat: {id: -266372953},
                from: {id: 1111111, first_name: 'FifaBot'}
            }
    },
    {
        message:
            {
                text: '#wc2018 adminprediction 202573088 Mohammadamin ARG-ISL 2-1',
                chat: {id: -266372953},
                from: {id: 1111111, first_name: 'FifaBot'}
            }
    },
    {
        message:
            {
                text: '#wc2018 adminprediction 91341610 Alireza ARG-ISL 3-1',
                chat: {id: -266372953},
                from: {id: 1111111, first_name: 'FifaBot'}
            }
    },
    {
        message:
            {
                text: '#wc2018 adminprediction 91711331 alikhadem ARG-ISL 2-0',
                chat: {id: -266372953},
                from: {id: 1111111, first_name: 'FifaBot'}
            }
    },
    {
        message:
            {
                text: '#wc2018 adminprediction 111879932 Zeinab DEN-PER 1-0',
                chat: {id: -266372953},
                from: {id: 1111111, first_name: 'FifaBot'}
            }
    },
    {
        message:
            {
                text: '#wc2018 adminprediction 105508663 Rasool DEN-PER 3-0',
                chat: {id: -266372953},
                from: {id: 1111111, first_name: 'FifaBot'}
            }
    },
    {
        message:
            {
                text: '#wc2018 adminprediction 74458279 Mojtaba DEN-PER 0-1',
                chat: {id: -266372953},
                from: {id: 1111111, first_name: 'FifaBot'}
            }
    },
    {
        message:
            {
                text: '#wc2018 adminprediction 108881239 Mina DEN-PER 1-0',
                chat: {id: -266372953},
                from: {id: 1111111, first_name: 'FifaBot'}
            }
    },
    {
        message:
            {
                text: '#wc2018 adminprediction 75043130 MahnAz DEN-PER 1-1',
                chat: {id: -266372953},
                from: {id: 1111111, first_name: 'FifaBot'}
            }
    },
    {
        message:
            {
                text: '#wc2018 adminprediction 340961698 Kimia DEN-PER 1-0',
                chat: {id: -266372953},
                from: {id: 1111111, first_name: 'FifaBot'}
            }
    },
    {
        message:
            {
                text: '#wc2018 adminprediction 105348277 Faegheh DEN-PER 2-0',
                chat: {id: -266372953},
                from: {id: 1111111, first_name: 'FifaBot'}
            }
    },
    {
        message:
            {
                text: '#wc2018 adminprediction 379150329 Asal DEN-PER 1-0',
                chat: {id: -266372953},
                from: {id: 1111111, first_name: 'FifaBot'}
            }
    },
    {
        message:
            {
                text: '#wc2018 adminprediction 202573088 Mohammadamin DEN-PER 2-1',
                chat: {id: -266372953},
                from: {id: 1111111, first_name: 'FifaBot'}
            }
    },
    {
        message:
            {
                text: '#wc2018 adminprediction 91341610 Alireza DEN-PER 1-1',
                chat: {id: -266372953},
                from: {id: 1111111, first_name: 'FifaBot'}
            }
    },
    {
        message:
            {
                text: '#wc2018 adminprediction 91711331 alikhadem DEN-PER 1-1',
                chat: {id: -266372953},
                from: {id: 1111111, first_name: 'FifaBot'}
            }
    },
    {
        message:
            {
                text: '#wc2018 adminprediction 91711331 alikhadem CRO-NGA 1-0',
                chat: {id: -266372953},
                from: {id: 1111111, first_name: 'FifaBot'}
            }
    },
    {
        message:
            {
                text: '#wc2018 adminprediction 91341610 Alireza CRO-NGA 2-0',
                chat: {id: -266372953},
                from: {id: 1111111, first_name: 'FifaBot'}
            }
    },
    {
        message:
            {
                text: '#wc2018 adminprediction 202573088 Mohammadamin CRO-NGA 2-1',
                chat: {id: -266372953},
                from: {id: 1111111, first_name: 'FifaBot'}
            }
    },
    {
        message:
            {
                text: '#wc2018 adminprediction 379150329 Asal CRO-NGA 2-0',
                chat: {id: -266372953},
                from: {id: 1111111, first_name: 'FifaBot'}
            }
    },
    {
        message:
            {
                text: '#wc2018 adminprediction 105348277 Faegheh CRO-NGA 1-1',
                chat: {id: -266372953},
                from: {id: 1111111, first_name: 'FifaBot'}
            }
    },
    {
        message:
            {
                text: '#wc2018 adminprediction 340961698 Kimia CRO-NGA 0-0',
                chat: {id: -266372953},
                from: {id: 1111111, first_name: 'FifaBot'}
            }
    },
    {
        message:
            {
                text: '#wc2018 adminprediction 75043130 MahnAz CRO-NGA 0-0',
                chat: {id: -266372953},
                from: {id: 1111111, first_name: 'FifaBot'}
            }
    },
    {
        message:
            {
                text: '#wc2018 adminprediction 108881239 Mina CRO-NGA 1-2',
                chat: {id: -266372953},
                from: {id: 1111111, first_name: 'FifaBot'}
            }
    },
    {
        message:
            {
                text: '#wc2018 adminprediction 74458279 Mojtaba CRO-NGA 1-0',
                chat: {id: -266372953},
                from: {id: 1111111, first_name: 'FifaBot'}
            }
    },
    {
        message:
            {
                text: '#wc2018 adminprediction 105508663 Rasool CRO-NGA 1-2',
                chat: {id: -266372953},
                from: {id: 1111111, first_name: 'FifaBot'}
            }
    },
    {
        message:
            {
                text: '#wc2018 adminprediction 111879932 Zeinab CRO-NGA 0-0',
                chat: {id: -266372953},
                from: {id: 1111111, first_name: 'FifaBot'}
            }
    },
    {
        message:
            {
                text: '',
                chat: {id: -266372953},
                from: {id: 1111111, first_name: 'FifaBot'}
            }
    }
];




// var message = Parse.parseMessage(prev_predictions[0]);
// DB.main(message);

for (var i=1; i<prev_predictions.length; i++) {
    var message = Parse.parseMessage(prev_predictions[i]);
    DB.main(message);
}