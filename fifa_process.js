// parses a message and decides a human readable return + a prediction record if it contains one
// or game. Returns a response in a record
var validIds = [-266372953, -1]
var parseMessage = exports.parseMessage = function (msg) {
//parseMessage = function (msg) {
    var response = {
        chat_group_id: msg.message.chat.id,
        is_valid: false,
        type: "unknown",  // "prediction", "game", "result", "help", "table"
        prediction: {},
        game: {},
        text: ""
    };
    
    
    var R = /(\w|\s)*\w(?=")|\w+/g;
    var chatGroupID = msg.message.chat.id;
    
    if (true) { // validIds.includes(chatGroupID)) {
        var msgText = msg.message.text;
        var user_id = msg.message.from.id;
        var username = msg.message.from.first_name;
        if (typeof(msgText) === 'string') {
            var msgArray = msgText.match(R);
            if (msgArray === null) {
                return response
            }
            if (msgArray[0] !== "wc2018") {// ignore everything that does not mention @khadem_fifa_bot
                return (response);
            }
            
            response.is_valid = true;
            
            if (msgArray.length === 1) {
                response.text = "Hmm?";
                return (response);
            }
            
            // now let's parse what else is going on for special commands
            if (msgArray[1] === "game") {
                // this is a special command for adding a game.
                if (createAddGameResponse(msgArray, response.game)) {
                    response.is_valid = true;
                    response.type = "game";
                    response.text = "Added game";
                }
                return response;
            }
            
            if (msgArray[1] === "result") {
                // this is a special command for adding a game.
                if (createResultResponse(msgArray, response.game)) {
                    response.is_valid = true;
                    response.type = "result";
                    response.text = "Updated game.";
                }
                return response;
            }
            
            if (msgArray[1] === "delete") {
                // this is a special command for adding a game.
                if (createDeleteGameResponse(msgArray, response.game)) {
                    response.is_valid = true;
                    response.type = "delete";
                    response.text = "deleted game.";
                }
                return response;
            }
            
            
            if (msgArray[1] === "table") {
                // this is a special command for adding a game.
                response.is_valid = true;
                response.type = "leaderboard";
                response.text = "Generating lead board.";
                return response;
            }
            
            if (msgArray[1] === "register") {
                // this is a special registering.
                response.is_valid = true;
                response.type = "register";
                response.user_id = user_id;
                response.text = "Thanks for registering. You are in!";
                return response;
            }
            
            
            if (msgArray[1] === "adminprediction") {
                // this is a special command for adding a game.
                if (createAdminPredictionResponse(msgArray, response)) {
                    response.is_valid = true;
                    response.type = "adminprediction";
                    response.text = "Updated prediction for " + response.prediction.name;
                }
                return response;
            }
            
            var parseResult = createPredictionResponse(msgArray, response.prediction);
            if (parseResult) {
                response.is_valid = true;
                response.type = "prediction";
                response.prediction.user_id = user_id;
                response.prediction.name = username;
                response.text = "Accepted your prediction";
            } else {
                response.is_valid = true;
                response.type = "unknown";
                response.text = "Invalid command! Prediction needs to be team1-team2 score1-score2, where team1 and team2 are valid three-letter country acronyms.";
            }
        }
    }
    
    return (response);
};

function createAddGameResponse(msgArray, result) {
    // msgArray has first tokens
    if (msgArray.length !== 5) {
        return false;
    }
    
    if (msgArray[2] < msgArray[3]) {
        var team1 = msgArray[2];
        var team2 = msgArray[3];
    } else {
        team1 = msgArray[3];
        team2 = msgArray[2];
    }
    
    result.team1 = team1.toUpperCase();
    result.team2 = team2.toUpperCase();
    
    var gameTime = parseInt(msgArray[4]);
    if (!isNaN(gameTime)) {
        result.time = gameTime;
        result.goal1 = 99;
        result.goal2 = 99;
        return true;
    }
    
    return false;
}

function createDeleteGameResponse(msgArray, result) {
    // msgArray has first tokens
    if (msgArray.length !== 4) {
        return false;
    }
    
    if (msgArray[2] < msgArray[3]) {
        var team1 = msgArray[2];
        var team2 = msgArray[3];
    } else {
        team1 = msgArray[3];
        team2 = msgArray[2];
    }
    
    result.team1 = team1.toUpperCase();
    result.team2 = team2.toUpperCase();
    return true;
}


function createResultResponse(msgArray, response) {
    // msgArray has first tokens
    if (msgArray.length !== 6) {
        return false;
    }
    
    var subMsg = msgArray.slice(2);
    var resultOk = extractGameAndResult(subMsg, response);
    return resultOk;
}

function extractGameAndResult(msgArray, result) {
    if (msgArray.length !== 4) {
        return false;
    }
    
    var goal1 = parseInt(msgArray[2]);
    var goal2 = parseInt(msgArray[3]);
    
    if (!isNaN(goal1) && !isNaN(goal2)) {
        if (msgArray[0] < msgArray[1]) {
            var team1 = msgArray[0];
            var team2 = msgArray[1];
        } else {
            team1 = msgArray[1];
            team2 = msgArray[0];
            goal1 = parseInt(msgArray[3]);
            goal2 = parseInt(msgArray[2]);
        }
        result.team1 = team1.toUpperCase();
        result.team2 = team2.toUpperCase();
        result.goal1 = goal1;
        result.goal2 = goal2;
        return (true);
    }
    return (false);
}


var parseAllPredictions = exports.parseAllPredictions = function (jsonObj, validChatGroupID) {
    var response = [];
    for (var i = 0; i < jsonObj.result.length; i++) {
        var result = parseMessage(jsonObj.result[i], validChatGroupID);
        if (result.is_valid && result.type === "prediction") {
            response.push(result.prediction);
        }
    }
    
    return (response);
}

var parseAllGames = exports.parseAllResults = function (jsonObj, validChatGroupID) {
    var response = [];
    for (var i = 0; i < jsonObj.result.length; i++) {
        var result = parseMessage(jsonObj.result[i], validChatGroupID);
        if (result.is_valid === true && result.type === "result") {
            response.push(result.game);
        }
    }
    
    return (response);
}


function scores(predictions, results) {
    var scoreTable = [];
    // need to double-check that each user_id only has predicted one result. And if more than one, pick the last one
    for (var i = 0; i < predictions.length; i++) {
        var prediction = predictions[i];
        var user_id = prediction.user_id;
        var user_name = prediction.name;
        // look for games and results
        for (var j = 0; j < results.length; j++) {
            var game = results[j];
            if (prediction.team1 === game.team1 && prediction.team2 === game.team2) {
                if (game.goal1 !== 99) {
                    var predictionScore = 0;
                    if (game.goal1 === prediction.goal1) {
                        predictionScore += 1;
                    }
                    
                    if (game.goal2 === prediction.goal2) {
                        predictionScore += 1;
                    }
                    
                    if ((game.goal1 - game.goal2) === (prediction.goal1 - prediction.goal2)) {
                        predictionScore += 1;
                    }
                    
                    if ((game.goal1 > game.goal2 && prediction.goal1 > prediction.goal2) ||
                        (game.goal1 < game.goal2 && prediction.goal1 < prediction.goal2) ||
                        (game.goal1 === game.goal2 && prediction.goal1 === prediction.goal2)) {
                        predictionScore += 1;
                    }
                    
                    predictionScore *= scoreWeightFactor(game.time);
                    
                    
                    var scoreItem = {
                        "user_id": user_id.toString(),
                        "name": user_name,
                        "game_id": game.team1 + game.team2,
                        "score": predictionScore
                    };
                    
                    // if this score time exists for the same game ignore it, the previous one is acceptable
                    
                    scoreTable.push(scoreItem);
                }
            }
        }
    }
    
    pruneScoreTable(scoreTable);
    
    return (scoreTable);
}

function scoreWeightFactor(gameTime)
{
    var weight = 1.0;
    
    if (gameTime > 1530342022 && gameTime < 1530774000) {
        weight = 1.4;
    }
    
    if (gameTime > 1530774000 && gameTime < 1531033200) {
        weight = 1.8;
    }
    
    if (gameTime > 1531033200 && gameTime < 1531638000) {
        weight = 2.3;
    }
    
    if (gameTime > 1531638000 && gameTime < 1531724400) {
        weight = 3;
    }
    
    
    
    return (weight);
}

function pruneScoreTable(scoreTable) {
    // look to see if this is duplicate, if so, replace it, if not add it to the table
    scoreTable.reverse();
    for (var i = 0; i < scoreTable.length; i++) {
        var scoreItem = scoreTable[i];
        for (var j = i + 1; j < scoreTable.length; j++) {
            if (scoreItem.user_id === scoreTable[j].user_id && scoreItem.game_id === scoreTable[j].game_id) {
                scoreTable.splice(j, 1);
                j--;
            }
        }
    }
    
}

var buildLadderTable = exports.buildLadderTable = function (predictions, results) {
    var ladderTable = [];
    
    var scoreTable = scores(predictions, results);
    
    for (var i = 0; i < scoreTable.length; i++) {
        var scoreRecord = scoreTable[i];
        var user_id = scoreRecord.user_id;
        var user_name = scoreRecord.name;
        
        // find if this user is already in the table, if so, update the score for him, otherwise, add a new entity
        var userFound = false;
        for (var j = 0; j < ladderTable.length; j++) {
            if (ladderTable[j].user_id === user_id) {
                var newScore = ladderTable[j].score += scoreRecord.score;
                ladderTable.splice(j, 1, {user_id: user_id, name: user_name, score: newScore});
                userFound = true;
                break;
            }
        }
        
        if (!userFound) {
            ladderTable.push({
                user_id: user_id,
                name: user_name,
                score: scoreRecord.score
            });
        }
    }
    
    // sort laderTable
    ladderTable.sort(compareRecords);
    
    var prettyTable = prettyPrintLadderTable(ladderTable);
    
    return prettyTable;
}

exports.isPredictionValid = function (predictionRecord, games) {
    var result = false;
    // first see if this prediction is a valid game
    var prediction = predictionRecord.prediction;
    for (var i = 0; i < games.length; i++) {
        var game = games[i];
        // console.log("here");
        if (prediction.team1 === game.team1 && prediction.team2 === game.team2) {
            console.log("here");
            var currentTime = Date.now() / 1000;
            console.log(currentTime);
            console.log(game.time - 30 * 60);
            if (currentTime < game.time - 30 * 60) {
                console.log("here");
                // the window of prediction is ok.
                result = true;
                predictionRecord.text = "Accepted your prediction. Good luck!";
                break;
            } else {
                // the time is too late for prediction
                result = false;
                predictionRecord.text = "Sorry! It's too late.";
                break;
            }
        } else {
            result = false;
            predictionRecord.text = "The game is not valid!"
        }
        
    }
    
    return (result);
}

function compareRecords(a, b) {
    return (b.score - a.score);
}

var countryList = ['BRA', 'GER', 'IRN', 'MOR'];


function createPredictionResponse(msgArray, result) {
    var parseOk = false;
    if (msgArray.length !== 5) {
        return parseOk;
    }
    
    parseOk = extractGameAndResult(msgArray.slice(1), result);
    return (parseOk);
}

function createAdminPredictionResponse(msgArray, response) {
    var parseOk = false;
    if (msgArray.length !== 8) {
        return parseOk;
    }
    
    parseOk = extractGameAndResult(msgArray.slice(4), response.prediction);
    //response.prediction.time = Date.now() / 1000;
    response.prediction.user_id = msgArray[2];
    response.prediction.name = msgArray[3];
    
    return (parseOk);
}

var prettyPrintLadderTable = exports.prettyPrintLadderTable = function (ladderTable) {
    var output = "Ladder Table:\n";
    
    for (var i = 0; i < ladderTable.length; i++) {
        output += ((i + 1) + "- " + ladderTable[i].name + "    " + Math.round(ladderTable[i].score) + "\n");
    }
    
    return (output);
}
