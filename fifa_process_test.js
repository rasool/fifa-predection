var Parser = require('./fifa_process');

var jsonObj = {
    "ok": true,
    "result":
        [
            {
                "update_id": 431304609,
                "message":
                    {
                        "message_id": 1,
                        "from": {"id": 105508663, "is_bot": false, "first_name": "Rasool", "username": "rasoolkhadem"},
                        "chat":
                            {
                                "id": -266372953,
                                "title": "Test fifa",
                                "type": "group",
                                "all_members_are_administrators": true
                            },
                        "date": 1528945697,
                        "new_chat_participant":
                            {
                                "id": 619805132,
                                "is_bot": true,
                                "first_name": "KhademFifaBot",
                                "username": "khadem_fifa_bot"
                            },
                        "new_chat_member":
                            {
                                "id": 619805132,
                                "is_bot": true,
                                "first_name": "KhademFifaBot",
                                "username": "khadem_fifa_bot"
                            },
                        "new_chat_members":
                            [
                                {
                                    "id": 619805132,
                                    "is_bot": true,
                                    "first_name": "KhademFifaBot",
                                    "username": "khadem_fifa_bot"
                                }
                            ]
                    }
            },
            {
                "update_id": 431304610,
                "message":
                    {
                        "message_id": 3,
                        "from": {"id": 105508663, "is_bot": false, "first_name": "Rasool", "username": "rasoolkhadem"},
                        "chat":
                            {
                                "id": -266372953,
                                "title": "Test fifa",
                                "type": "group",
                                "all_members_are_administrators": true
                            },
                        "date": 1528946643,
                        "text": "test1"
                    }
            },
            {
                "update_id": 431304611,
                "message": {
                    "message_id": 4,
                    "from": {"id": 105508663, "is_bot": false, "first_name": "Rasool", "username": "rasoolkhadem"},
                    "chat": {
                        "id": -266372953,
                        "title": "Test fifa",
                        "type": "group",
                        "all_members_are_administrators": true
                    },
                    "date": 1528946650,
                    "text": "#wc2018   #result GER-BRA 2-1",
                    "entities": [{"offset": 0, "length": 16, "type": "mention"}]
                }
            },
            {
                "update_id": 431304612,
                "message": {
                    "message_id": 5,
                    "from": {"id": 617100521, "is_bot": false, "first_name": "Soroush", "last_name": "Khadem"},
                    "chat": {
                        "id": -266372953,
                        "title": "Test fifa",
                        "type": "group",
                        "all_members_are_administrators": true
                    },
                    "date": 1528946690,
                    "text": "BRA-GER 2-1"
                }
            },
            {
                "update_id": 431304613,
                "message": {
                    "message_id": 6,
                    "from": {"id": 105508663, "is_bot": false, "first_name": "Rasool", "username": "rasoolkhadem"},
                    "chat": {
                        "id": -266372953,
                        "title": "Test fifa",
                        "type": "group",
                        "all_members_are_administrators": true
                    },
                    "date": 1528946731,
                    "text": "#wc2018 GER-BRA 2-1",
                    "entities": [{"offset": 0, "length": 16, "type": "mention"}]
                }
            },
            {
                "update_id": 431304614,
                "message": {
                    "message_id": 7,
                    "from": {"id": 617100521, "is_bot": false, "first_name": "Soroush", "last_name": "Khadem"},
                    "chat": {
                        "id": -266372953,
                        "title": "Test fifa",
                        "type": "group",
                        "all_members_are_administrators": true
                    },
                    "date": 1528946966,
                    "text": "#wc2018 #game GER-IRN 1529131962",
                    "entities": [{"offset": 0, "length": 16, "type": "mention"}]
                }
            },
            {
                "update_id": 431304615,
                "message": {
                    "message_id": 8,
                    "from": {"id": 105508663, "is_bot": false, "first_name": "Rasool", "username": "rasoolkhadem"},
                    "chat": {
                        "id": -266372953,
                        "title": "Test fifa",
                        "type": "group",
                        "all_members_are_administrators": true
                    },
                    "date": 1528947157,
                    "text": "#wc2018 POR-IRN 2-2"
                }
            },
            {
                "update_id": 431304616,
                "message": {
                    "message_id": 9,
                    "from": {"id": 617100521, "is_bot": false, "first_name": "Soroush", "last_name": "Khadem"},
                    "chat": {
                        "id": -266372953,
                        "title": "Test fifa",
                        "type": "group",
                        "all_members_are_administrators": true
                    },
                    "date": 1528947864,
                    "text": "#wc2018  POR - IRN 0-4"
                }
            },
            {
                "update_id": 431304617,
                "message": {
                    "message_id": 10,
                    "from": {"id": 617100521, "is_bot": false, "first_name": "Soroush", "last_name": "Khadem"},
                    "chat": {
                        "id": -266372953,
                        "title": "Test fifa",
                        "type": "group",
                        "all_members_are_administrators": true
                    },
                    "date": 1528947873,
                    "text": "#wc2018 IRN-GER 1-2"
                }
            },
            {
                "update_id": 431304618,
                "message": {
                    "message_id": 11,
                    "from": {"id": 617100521, "is_bot": false, "first_name": "Soroush", "last_name": "Khadem"},
                    "chat": {
                        "id": -266372953,
                        "title": "Test fifa",
                        "type": "group",
                        "all_members_are_administrators": true
                    },
                    "date": 1528947874,
                    "text": "#wc2018 BRA-GER 1-1"
                }
            },
            {
                "update_id": 431304619,
                "message": {
                    "message_id": 12,
                    "from": {"id": 617100521, "is_bot": false, "first_name": "Soroush", "last_name": "Khadem"},
                    "chat": {
                        "id": -266372953,
                        "title": "Test fifa",
                        "type": "group",
                        "all_members_are_administrators": true
                    },
                    "date": 1528947875,
                    "text": "alfh"
                }
            },
            {
                "update_id": 431304620,
                "message": {
                    "message_id": 13,
                    "from": {"id": 105508663, "is_bot": false, "first_name": "Rasool", "username": "rasoolkhadem"},
                    "chat": {
                        "id": -266372953,
                        "title": "Test fifa",
                        "type": "group",
                        "all_members_are_administrators": true
                    },
                    "date": 1528949449,
                    "text": "#wc2018 MAR-GER 1-0"
                }
            },
            {
                "update_id": 431304620,
                "message": {
                    "message_id": 13,
                    "from": {"id": '105508663', "is_bot": false, "first_name": "Rasool", "username": "rasoolkhadem"},
                    "chat": {
                        "id": -266372953,
                        "title": "Test fifa",
                        "type": "group",
                        "all_members_are_administrators": true
                    },
                    "date": 1528949449,
                    "text": "#wc2018 BRA-GER 1-2"
                }
            },
            {
                "update_id": 431304620,
                "message": {
                    "message_id": 13,
                    "from": {"id": '105508699', "is_bot": false, "first_name": "Soha", "username": "rasoolkhadem"},
                    "chat": {
                        "id": -266372953,
                        "title": "Test fifa",
                        "type": "group",
                        "all_members_are_administrators": true
                    },
                    "date": 1528949449,
                    "text": "#wc2018 IRN MAR 0-0"
                }
            },
            {
                "update_id": 431304620,
                "message": {
                    "message_id": 13,
                    "from": {"id": 105508663, "is_bot": false, "first_name": "Rasool", "username": "rasoolkhadem"},
                    "chat": {
                        "id": -266372953,
                        "title": "Test fifa",
                        "type": "group",
                        "all_members_are_administrators": true
                    },
                    "date": 1528949449,
                    "text": "#wc2018 BRA-GER 1-1"
                }
            },
            {
                "update_id": 431304620,
                "message": {
                    "message_id": 13,
                    "from": {"id": 105508664, "is_bot": false, "first_name": "Mina", "username": "rasoolkhadem"},
                    "chat": {
                        "id": -266372953,
                        "title": "Test fifa",
                        "type": "group",
                        "all_members_are_administrators": true
                    },
                    "date": 1528949449,
                    "text": "#wc2018 IRN MAR 1-0"
                }
            },
            {
                "update_id": 431304621,
                "message": {
                    "message_id": 14,
                    "from": {"id": 105508778, "is_bot": false, "first_name": "Person1", "username": "rasoolkhadem"},
                    "chat": {
                        "id": -266372953,
                        "title": "Test fifa",
                        "type": "group",
                        "all_members_are_administrators": true
                    },
                    "date": 1528949502,
                    "text": "#wc2018 #game RUS-SAU 1528949502",
                    "entities": [{"offset": 0, "length": 16, "type": "mention"}]
                }
            },
            {
                "update_id": 431304621,
                "message": {
                    "message_id": 14,
                    "from": {"id": 105508778, "is_bot": false, "first_name": "Person1", "username": "rasoolkhadem"},
                    "chat": {
                        "id": -266372953,
                        "title": "Test fifa",
                        "type": "group",
                        "all_members_are_administrators": true
                    },
                    "date": 1528949502,
                    "text": "#wc2018 #game MAR-IRN 1528949502",
                    "entities": [{"offset": 0, "length": 16, "type": "mention"}]
                }
            },
            {
                "update_id": 431304621,
                "message": {
                    "message_id": 14,
                    "from": {"id": 105508778, "is_bot": false, "first_name": "Rasool", "username": "rasoolkhadem"},
                    "chat": {
                        "id": -266372953,
                        "title": "Test fifa",
                        "type": "group",
                        "all_members_are_administrators": true
                    },
                    "date": 1528949502,
                    "text": "#wc2018 result MAR-IRN 0 0",
                    "entities": [{"offset": 0, "length": 16, "type": "mention"}]
                }
            },
            {
                "update_id": 431304621,
                "message": {
                    "message_id": 14,
                    "from": {"id": 105508778, "is_bot": false, "first_name": "Rasool", "username": "rasoolkhadem"},
                    "chat": {
                        "id": -266372953,
                        "title": "Test fifa",
                        "type": "group",
                        "all_members_are_administrators": true
                    },
                    "date": 1528949502,
                    "text": "#wc2018 result GER-MEX 0 1",
                    "entities": [{"offset": 0, "length": 16, "type": "mention"}]
                }
            }
        
        
        ]
};


var sampMsg = {
    "update_id": 431304700,
    "message":
        {
            "message_id": 154,
            "from": {
                "id": 617100521, "is_bot": false, "first_name": "Soroush", "last_name": "Khadem"
            },
            "chat": {"id": -266372953, "title": "Test fifa", "type": "group", "all_members_are_administrators": true},
            "date": 1529029356,
            "text": "qiewh"
        }
};

function readCommandText(fileName)
{
    var fs = require('fs');
    var path = require('path');
    var readStream = fs.createReadStream(path.join(__dirname, '') + fileName, 'utf8');
    var data = '';
    readStream.on('data', function(chunk) {
    data += chunk;
    data = data.split(/\r?\n/g);
    var allRecords = [];
    
    console.log('[');
    for (var i = 0; i < data.length; i++) {
    
        var msg = generateSimpleRecordFromText(data[i]);
        console.log(msg, '$$');
        allRecords.push(msg);
    }
    console.log(']');
//    console.log(allRecords);
    }).on('end', function() {
    });
    
    
    return (data);
}

function generateSimpleRecordFromText(txt)
{
    //var txtArr = txt.split(',');
    var msg = {
        message: {
            text: txt,
            chat: {
                id: -266372953
            },
            from: {
                id: 1111111,
                first_name: 'FifaBot'
            }
        }
    };
    
    return (msg);
}

//function initApp()
{
    //var obj = JSON.parse(jsonText);
    // var parseResult = createPredictionResponse(obj);
    var chatGroupID = -266372953;  // needs to change based on khadem family group ID
    
    var predictions = Parser.parseAllPredictions(jsonObj, chatGroupID);
    
    for (var i = 0; i < predictions.length; i++) {
        var p = predictions[i];
        console.log(p.name, "- ", p.team1, "-", p.team2, "    ", p.goal1, "-", p.goal2);
    }
    //console.log(predictions);
    
    var results = Parser.parseAllResults(jsonObj, chatGroupID);
    console.log("Game Results:");
    for (i = 0; i < results.length; i++) {
        console.log(results[i].team1, "-", results[i].team2, "  ", results[i].goal1, results[i].goal2);
    }
    //console.log(results);
    
    var scoreTable = Parser.buildLadderTable(predictions, results);
    console.log(scoreTable);
    
    //var data = readCommandText('/predictionimport.txt');
    //data.split("\n");
    //console.log(data.length);
    
    //var msg = generateSimpleRecordFromText(data);
    //console.log(msg);
}